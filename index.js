const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const cors = require('cors');

const app = express();

var currentTurnamentID = 0;
var trophyCount = 0;
var messageID = -1;

app.listen(8080, () => {
    console.log('Server started on port 8080...');
})

app.use(bodyParser.json());
app.use(cors());

const sequelize = new Sequelize('c9', 'sanduac', '', {
    host: 'localhost',
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database: ', err);
    });


const User = sequelize.define('users', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    score: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    turnamentId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    type: {
        type: Sequelize.STRING,
        allowNull: false
    },
    currentTurnamentScore: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

const Message = sequelize.define('messages', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    message: {
        type: Sequelize.STRING,
        allowNull: false
    },
    turnamentID: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    userId: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

const Turnament = sequelize.define('turnaments', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    id: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true
    },
    score: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    ended: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
});

const Trophy = sequelize.define('trophy', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    userId: {
        type: Sequelize.STRING,
        allowNull: false
    },
    turnamentID: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    score: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

sequelize.sync({ force: true }).then(() => {
    console.log('Databases create successfully')
})







//APIs
app.post('/register', (req, res) => {
    User.create({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        turnamentId: "0",
        score: "0",
        type: req.body.type,
        currentTurnamentScore: "0"
    }).then((user) => {
        sendJsonResponse(res, true, "User created successfully", null, 200);
    }, (err) => {
        sendJsonResponse(res, false, err["errors"][0]["message"], null, 206);
    })
})

app.post('/login', (req, res) => {
    User.findOne({ where: { username: req.body.username, password: req.body.password } }).then((result) => {
        if (result != null) {
            sendJsonResponse(res, true, "Successfully logged in!", result, 200)
        }
        else {
            sendJsonResponse(res, false, "Invalid Credentials!", null, 206);
        }
    })
});

app.post('/incrementscore', (req, res) => {
    // //TODO Same with createTurnament
    // if(!userIsAdmin(req.body.username)){
    //     sendJsonResponse(res, false, "User is not admin!", null, 206);
    //     return;
    // }

    User.findOne({ where: { username: req.body.username } }).then((user) => {
        if (user != null) {
            if (user.type == "admin") {
                User.findOne({ where: { username: req.body.usernameToIncrement } }).then((result) => {
                    if (result != null) {
                        var score = parseInt(result.score);
                        var scoreToIncrement = parseInt(req.body.score);
                        score = score + scoreToIncrement;
                        var usernameVar = req.body.username;
                        var sql = "UPDATE users SET score = " + score + " WHERE username = '" + usernameVar + "';";
                        sequelize.query(sql, function(err, result) {
                            if (err == null) {
                                sendJsonResponse(res, true, "Score successfully updated!", null, 200);
                            }
                            else {
                                sendJsonResponse(res, false, "Some problem occured!", null, 206);
                            }
                        });
                        sendJsonResponse(res, true, "Score successfully updated!", null, 200);
                    }
                    else {
                        sendJsonResponse(res, false, "Invalid username", null, 206);
                    }
                })

            }
            else {
                sendJsonResponse(res, false, "User is not admin!", null, 206);
            }
        }
        else {
            sendJsonResponse(res, false, "User is not admin!", null, 206);
        }
    })


})

app.post('/incrementcurrentturnamentscore', (req, res) => {
    User.findOne({ where: { username: req.body.username } }).then((user) => {
        if (user != null) {
            Turnament.findOne({ where: { id: currentTurnamentID } }).then((turnament) => {
                if (turnament != null) {
                    if (turnament.ended == true) {
                        sendJsonResponse(res, false, "There is no ongoing turnament!", null, 206);
                    }
                    else {
                        if (turnament.id == user.turnamentId) {
                            var score = parseInt(user.currentTurnamentScore) + parseInt(req.body.score);
                            var sql = "UPDATE users SET currentTurnamentScore = '" + score + "' WHERE username='" + req.body.username + "';";
                            sequelize.query(sql, function(err, result) {});
                            sendJsonResponse(res, true, "Score successfully updated!", null, 206);
                        }
                        else {
                            sendJsonResponse(res, false, "The user is not enrolled in the current turnament!", null, 206);
                        }
                    }
                }
                else {
                    sendJsonResponse(res, false, "The user is not enrolled in the current turnament!", null, 206);
                }
            })
        }
        else {
            sendJsonResponse(res, false, "Invalid username", null, 206);
        }
    })
})

app.post('/createTurnament', (req, res) => {
    // //TODO don't know how to do this
    // var userIsAdmin = await userIsAdmin(req.body.username);
    // if(!userIsAdmin){
    //     sendJsonResponse(res, false, "User is not admin!", null, 206);
    //     return;
    // }
    // //I did it this way tho, it works but it's ugly af

    User.findOne({ where: { username: req.body.username } }).then((user) => {
        if (user != null) {
            if (user.type == "admin") {
                Turnament.findOne({ where: { id: currentTurnamentID } }).then((result) => {
                    if (result != null) {
                        if (result.ended == true) {
                            currentTurnamentID += 1;

                            var sql = "UPDATE users SET currentTurnamentScore = '" + 0 + "';";
                            sequelize.query(sql, function(err, result) {});
                            Turnament.create({
                                name: req.body.name,
                                id: currentTurnamentID,
                                score: parseInt(Math.random() * (1000 - 100) + 100),
                                ended: false
                            }).then((turnament) => {
                                sendJsonResponse(res, true, "Turnament successfully created!", turnament, 200);
                            }, (err) => {
                                sendJsonResponse(res, false, err, null, 206);
                            })
                        }
                        else {
                            sendJsonResponse(res, false, "There is already an ongoing turnament!", null, 206);
                        }
                    }
                    else {
                        currentTurnamentID += 1;
                        Turnament.create({
                            name: req.body.name,
                            id: currentTurnamentID,
                            score: parseInt(Math.random() * (1000 - 100) + 100),
                            ended: false
                        }).then((turnament) => {
                            sendJsonResponse(res, true, "Turnament successfully created!", turnament, 200);
                        }, (err) => {
                            sendJsonResponse(res, false, err, null, 206);
                        })
                    }
                })
            }
            else {
                sendJsonResponse(res, false, "User is not admin!", null, 206);
            }
        }
        else {
            sendJsonResponse(res, false, "User is not admin!", null, 206);
        }
    })
})

app.post('/getscore', (req, res) => {
    User.findOne({ where: { username: req.body.username } }).then((result) => {
        if (result != null) {
            var jsonData = {};
            jsonData["score"] = result.score;
            sendJsonResponse(res, true, "Successfully retrieved score", jsonData, 200);
        }
        else {
            sendJsonResponse(res, false, "Invalid username!", null, 206);
        }
    })
})

app.post('/getCurrentTurnament', (req, res) => {
    Turnament.findOne({ where: { id: currentTurnamentID } }).then((result) => {
        if (result != null) {
            sendJsonResponse(res, true, "Successfully retrieved turnament", result, 200);
        }
        else {
            sendJsonResponse(res, false, "Invalid username!", null, 206);
        }
    })
})

app.post('/enrollinturnament', (req, res) => {
    if (currentTurnamentID == 0) {
        sendJsonResponse(res, false, "There are no ongoing turnaments!", null, 206);
    }

    Turnament.findOne({ where: { id: currentTurnamentID } }).then((result) => {
        if (result != null) {
            if (!result.ended) {
                var usernameVar = req.body.username;
                var sql = "UPDATE users SET turnamentID = " + currentTurnamentID + " WHERE username = '" + usernameVar + "';";
                sequelize.query(sql, function(err, result) {});
                sendJsonResponse(res, true, "Successfully enrolled in turnament!", null, 200);
            }
            else {
                sendJsonResponse(res, false, "There are no ongoing turnaments", null, 206);
            }
        }
        else {
            sendJsonResponse(res, false, "There are no ongoing turnaments", null, 206);
        }
    })
})

app.post('/createmessage', (req, res) => {
    User.findOne({ where: { username: req.body.username } }).then((user) => {
        if (user != null) {
            messageID += 1;
            Message.create({
                id: messageID,
                turnamentID: currentTurnamentID,
                userId: req.body.username,
                message: req.body.message
            }).then((message) => {
                sendJsonResponse(res, true, "Message created successfully", null, 200);
            }, (err) => {
                sendJsonResponse(res, false, err, null, 206);
            })
        }
        else {
            sendJsonResponse(res, false, "User does not exist!", null, 206);
        }
    })
})
app.post('/getmessagesforturnament', (req, res) => {
    var localTurnamentId;
    if (req.body.turnamentID == undefined) {
        localTurnamentId = currentTurnamentID;
    }
    else {
        localTurnamentId = req.body.turnamentID;
    }
    Message.findAll({ where: { turnamentID: localTurnamentId } }).then((turnaments) => {
        if (turnaments.length) {
            sendJsonResponse(res, true, "Successfully retrieved all messages!", turnaments, 200)
        }
        else {
            sendJsonResponse(res, true, "This turnament does not have any messages!", turnaments, 200)
        }
    })
})

app.post('/gettrophies', (req, res) => {
    Trophy.findAll({ where: { userId: req.body.username } }).then((trophies) => {
        if (trophies.length) {
            sendJsonResponse(res, true, "Successfully retrieved trophies!", trophies, 200)
        }
        else {
            sendJsonResponse(res, true, "This user does not have any trophies yet!", trophies, 200)
        }
    })
})

app.post('/endTurnament', (req, res) => {
    // //TODO Same as create turnament
    // if(!userIsAdmin(req.body.username)){
    //     sendJsonResponse(res, false, "User is not admin!", null, 206);
    //     return;
    // }



    User.findOne({ where: { username: req.body.username } }).then((user) => {
        if (user != null) {
            if (user.type == "admin") {

                Turnament.findOne({ where: { id: currentTurnamentID } }).then((result) => {
                    if (result != null) {
                        var sql = "UPDATE turnaments SET ended = '1' WHERE id = '" + currentTurnamentID + "';";
                        sequelize.query(sql, function(err, result) {});
                        User.max('currentTurnamentScore', { where: { turnamentID: currentTurnamentID } }).then(currentScore => {
                            if (!isNaN(currentScore)) {
                                User.findOne({ where: { currentTurnamentScore: currentScore } }).then(user => {
                                    if (user != null) {
                                        var score = user.score;
                                        score += result.score;
                                        user.score += result.score;
                                        var sql2 = "UPDATE users SET score = '" + score + "' WHERE username = '" + user.username + "';";

                                        // const knex = 'knex';
                                        // knex.raw(
                                        //     `UPDATE users
                                        //     SET score = ?
                                        //     WHERE username = ?`,
                                        //     [score, user.username],
                                        // );


                                        sequelize.query(sql2, function(err, result) {});
                                        trophyCount += 1;
                                        Trophy.create({
                                            id: trophyCount,
                                            userId: user.username,
                                            turnamentID: currentTurnamentID,
                                            score: result.score
                                        }).then((trophy) => {
                                            sendJsonResponse(res, true, "Successfully ended turnament and gave the prizes!", null, 200);
                                        }, (err) => {
                                            sendJsonResponse(res, true, err, null, 206);
                                        })
                                    }
                                    else {
                                        sendJsonResponse(res, true, "Successfully ended turnament but no users were enrolled!", null, 200);
                                    }
                                })
                            }
                            else {
                                sendJsonResponse(res, true, "Successfully ended turnament but no users were enrolled!", null, 200);
                            }
                        })

                    }
                    else {
                        sendJsonResponse(res, false, "There are no ongoing turnaments", null, 206);
                    }
                })
            }
            else {
                sendJsonResponse(res, false, "User is not admin!", null, 206);
            }
        }
        else {
            sendJsonResponse(res, false, "User is not admin!", null, 206);
        }
    })

})

function sendJsonResponse(res, success, message, data, code) {
    var jsonResponse = {};
    jsonResponse["success"] = success;
    jsonResponse["message"] = message;
    jsonResponse["data"] = data;
    res.status(code).send(jsonResponse);
}




function userIsAdmin(usernameToFind) {
    User.findOne({ where: { username: usernameToFind } }).then((user) => {
        if (user != null) {
            if (user.type == "admin") {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    })
}
