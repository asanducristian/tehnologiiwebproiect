import React from 'react';
import axios from 'axios';
import './CreateTurnament.css'

export class CreateTurnament extends React.Component {
    constructor(props) {
        super(props);
        this.state={}
        this.state.user=[];
    }

    createTurnamentFunc = () => {
        if(document.getElementById('create_turnament_name').value==""){
            alert("All fields are mandatory");
            return;
        }
        
        let turnament = {
            username: this.state.user.username,
            name: document.getElementById('create_turnament_name').value
        }
        axios.post('https://proiect-web-sanduac.c9users.io/createTurnament', turnament).then((res) => {
            if(res.status === 200){
                console.log(res.data)
                alert("successfully create turnament")
            }
        }).catch((err) =>{
            console.log(err)
            alert("Failed")
        })
    }

    render() {
        this.state.user = this.props.source;
        return (
            <React.Fragment>
            <div id="create">
                <div id="create_layout">
                    <input id="create_turnament_name" type="text" placeholder="turnament name"/>
                    <button onClick={this.createTurnamentFunc}>CREATE</button>
                </div>
            </div>
            </React.Fragment>
        )
    }
}
