import React from 'react';
import axios from 'axios';
import './EndTurnament.css'

export class EndTurnament extends React.Component {
    constructor(props) {
        super(props);
        this.state={}
        this.state.user=[];
    }

    endTurnament = () => {
        var user={
            username: this.state.user.username
        }
        axios.post('https://proiect-web-sanduac.c9users.io/endTurnament', user).then((res) => {
            if(res.status === 200){
                console.log(res.data)
                alert("successfully ended turnament")
            }
        }).catch((err) =>{
            console.log(err)
            alert("Failed")
        })
    }

    render() {
        this.state.user = this.props.source;
        return (
            <React.Fragment>
            <div id="end">
                <div id="end_layout">
                    <button onClick={this.endTurnament}>END</button>
                </div>
            </div>
            </React.Fragment>
        )
    }
}
