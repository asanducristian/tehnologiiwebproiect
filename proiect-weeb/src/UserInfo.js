import React from 'react';
import axios from 'axios';
import './UserInfo.css'

export class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.state.user = [];
    }


    showUserInfo = () => {
        var user_info_layout = document.getElementById('user_info_layout');
        if (user_info_layout.style.display == "none") {
            user_info_layout.style.display = "block"

            let user = {
                username: this.state.user.username,
                password: this.state.user.password
            }
            axios.post('https://proiect-web-sanduac.c9users.io/login', user).then((res) => {
                if (res.status === 200) {
                    console.log(res.data)
                    this.state.user = res.data.data
                    document.getElementById('user_name').innerHTML = this.state.user.name;
                    document.getElementById('user_score').innerHTML = this.state.user.score;
                    document.getElementById('user_username').innerHTML = this.state.user.username;
                    document.getElementById('user_current_score').innerHTML = this.state.user.currentTurnamentScore;
                    document.getElementById('user_email').innerHTML = this.state.user.email;

                    let user = {
                        username: this.state.user.username
                    }
                    axios.post('https://proiect-web-sanduac.c9users.io/gettrophies', user).then((res) => {
                        if (res.status === 200) {
                            console.log(res.data)
                            let table = document.getElementById('trophies')
                            table.innerHTML = ""
                            let row = table.insertRow(0)
                            let cell1 = row.insertCell(0)
                            let cell2 = row.insertCell(1)

                            cell1.innerHTML = "Turnament ID"
                            cell2.innerHTML = "Score"
                            for (var i = 0; i < res.data.data.length; i++) {
                                let row = table.insertRow(i+1)
                                let cell1 = row.insertCell(0)
                                let cell2 = row.insertCell(1)

                                cell1.innerHTML = res.data.data[i].turnamentID
                                cell2.innerHTML = res.data.data[i].score
                            }

                        }
                    }).catch((err) => {
                        console.log(err)
                        alert("Invalid credentials")
                    })
                }
            }).catch((err) => {
                console.log(err)
                alert("Invalid credentials")
            })



        }
        else {
            user_info_layout.style.display = "none"
        }

    }

    render() {
        this.state.user = this.props.source;
        return (
            <React.Fragment>
            <div id="user_info_div">
                <button onClick={this.showUserInfo}>SHOW USER INFO</button>
                <div id="user_info_layout">
                    <p id="user_name">asdasdsa</p>
                    <p id="user_score"/>
                    <p id="user_username"/>
                    <p id="user_current_score"/>
                    <p id="user_email"/>
                    
                    <table id="trophies"></table>
                </div>
            </div>
            </React.Fragment>
        )
    }
}
