import React from 'react';
import axios from 'axios';
import './TurnamentInfo.css'

export class TurnamentInfo extends React.Component {
    constructor(props) {
        super(props);
    }


    getTurnamentInfo = () => {
        var turnament_info_layout = document.getElementById('turnament_info_layout');
        if (turnament_info_layout.style.display == "none") {
            turnament_info_layout.style.display = "block";

            axios.post('https://proiect-web-sanduac.c9users.io/getCurrentTurnament', []).then((res) => {
                if (res.status === 200) {
                    document.getElementById('turnament_name').innerHTML = "nume turneu: "+res.data.data.name;
                    document.getElementById('turnament_score').innerHTML = "scor turneu: "+res.data.data.score;
                    
                    console.log(res.data)
                }
            }).catch((err) => {
                console.log(err)
                alert("Invalid credentials")
            })
        }
        else {
            turnament_info_layout.style.display = "none";
        }
    }

    render() {
        return (
            <React.Fragment>
            <div id="turnament">
                <button onClick={this.getTurnamentInfo}>SHOW TURNAMENT INFO</button>
                <div id="turnament_info_layout">
                    <p id="turnament_name"/>
                    <p id="turnament_score"/>
                </div>
            </div>
            </React.Fragment>
        )
    }
}
