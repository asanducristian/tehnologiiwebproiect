import React, { Component } from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';


import { GameInfo } from './GameInfo'
import { Login } from './Login'
import { CreateTurnament } from './CreateTurnament'
import { EndTurnament } from './EndTurnament'
import { TurnamentInfo } from './TurnamentInfo'
import { UserInfo } from './UserInfo'
import { IncrementTurnament } from './IncrementTurnament'

class App extends Component {


  constructor(props) {
    super(props)
    this.state = {};
    this.state.user = [];
  }

  onLogIn = (userDataReceived) => {
    let userData = userDataReceived;
    this.setState({
      user: userData
    });
    document.getElementById("login").style.display = "none"
    if (this.state.user.type === "admin") {
      document.getElementById("create_turnament").style.display = "block"
      document.getElementById("end_turnament").style.display = "block"
    }
    else {
      document.getElementById("turnament_info").style.display = "block"
      document.getElementById("user_info").style.display = "block"
      document.getElementById("enroll_turnament").style.display = "block"
      document.getElementById("set_turnament_score").style.display = "block"

    }
  }
  
  logout =() =>{
    this.setState({
      user: []
    })
    
    document.getElementById("create_turnament").style.display = "none"
    document.getElementById("end_turnament").style.display = "none"
    document.getElementById("login").style.display = "block"
  }
  enroll =() =>{
    let user = {
            username: this.state.user.username,
        }
        axios.post('https://proiect-web-sanduac.c9users.io/enrollinturnament', user).then((res) => {
            if(res.status === 200){
                console.log(res.data)
                alert("successfully joined turnament")
            }
        }).catch((err) =>{
            console.log(err)
            alert("Failed")
        })
    
  }

  render() {
    return (
      <React.Fragment>
      <h1>Proiect Weeeeb</h1>
      <div id="gameInfo">
        <GameInfo />
      </div>
      <div id="login">
        <Login loggedIn={this.onLogIn}/>
      </div>
      <div id="create_turnament" className="first_hidden">
        CREATE TURNAMENT
        <CreateTurnament source={this.state.user}/>
      </div>
      <div id="end_turnament" className="first_hidden">
        END TURNAMENT
        <EndTurnament source={this.state.user}/>
      </div>
      <div id="turnament_info" className="first_hidden">
        <TurnamentInfo/>
      </div>
      <div id="user_info" className="first_hidden">
        <UserInfo source={this.state.user}/>
      </div>
      <div id="enroll_turnament" className="first_hidden">
        <button onClick={this.enroll}>JOIN CURRENT TURNAMENT</button>
      </div>
      <div id="set_turnament_score" className="first_hidden">
        <IncrementTurnament source={this.state.user}/>
      </div>
      
      
      
      
      
      
      
      
      <div id="logout">
        <button onClick={this.logout}>LOGOUT</button>
      </div>
      </React.Fragment>
    );
  }
}

export default App;
