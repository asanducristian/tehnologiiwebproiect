import React from 'react';
import axios from 'axios';
import './GameInfo.css'

export class GameInfo extends React.Component {

    constructor(props) {
        super(props);
    }

    showInfo = () => {
        var object = document.getElementById('game_info');
        if (object.style.display === "none") {
            object.style.display = "block";

            let row = `fields *; 
where id = 1940;
exclude tags;`
            axios.post('hhttps://api-v3.igdb.com/games/', row, {
                headers: {
                    'user-key': '43a9bd0266f9d648f85eb428b80d15ea',
                }
            }).then((res) => {
                if (res.status === 200) {
                    console.log(res.data)

                }
            }).catch((err) => {
                console.log(err)
                alert("Failed to load Game Data")
                object.style.display = "none";
            })
        }
        else {
            object.style.display = "none";
        }
    }


    render() {
        return (
            <React.Fragment>
            <button onClick={this.showInfo}>Show Game Info</button>
            <div id="game_info">
                <p id="game_name"/>
                <p id="game_summary"/>
                <p id="game_url"/>
                <p id="game_rating"/>
            </div>
            </React.Fragment>
        )
    }

}
