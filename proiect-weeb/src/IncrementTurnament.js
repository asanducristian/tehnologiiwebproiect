import React from 'react';
import axios from 'axios';
import './IncrementTurnament.css'

export class IncrementTurnament extends React.Component {

    constructor(props) {
        super(props);
        this.state={}
        this.state.user=[];
    }
    
    setScore = () => {
        if(document.getElementById('score').value==""){
            alert("All fields are mandatory")
            return
        }
        let score = {
            username: this.state.user.username,
            score: document.getElementById('score').value
        }
        axios.post('https://proiect-web-sanduac.c9users.io/incrementcurrentturnamentscore', score).then((res) => {
            if(res.status == 200){
                console.log(res.data)
                alert("successfully set score")
                document.getElementById('score').value=""
            }
        }).catch((err) =>{
            console.log(err)
            alert("invalid credentials")
        })
    }


    render() {
        this.state.user = this.props.source;
        return (
            <React.Fragment>
            <div id="increment">
                <div id="increment_layout">
                    <input id="score" type="text" placeholder="score"/>
                    <button onClick={this.setScore}>SET SCORE FOR TURNAMENT</button>
                </div>
            </div>
            </React.Fragment>
        )
    }

}
