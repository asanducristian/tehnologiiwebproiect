import React from 'react';
import axios from 'axios';
import './Login.css'

export class Login extends React.Component {

    constructor(props) {
        super(props);
    }

    showLogionLayout = () => {
        var login = document.getElementById('login_layout');
        var register = document.getElementById('register_layout');
        
        login.style.display = "block";
        register.style.display = "none";
    }

    showRegisterLayout = () => {
        var login = document.getElementById('login_layout');
        var register = document.getElementById('register_layout');
        
        login.style.display = "none";
        register.style.display = "block";
    }

    login = () => {
        if(document.getElementById('login_username').value==""){
            alert("All fields are mandatory")
            console.log("1")
            return
        }
        if(document.getElementById('login_password').value==""){
            alert("All fields are mandatory")
            console.log("2")
            return
        }
        let user = {
            username: document.getElementById('login_username').value,
            password: document.getElementById('login_password').value
        }
        axios.post('https://proiect-web-sanduac.c9users.io/login', user).then((res) => {
            if(res.status === 200){
                console.log(res.data)
                this.props.loggedIn(res.data.data)
            }
        }).catch((err) =>{
            console.log(err)
            alert("Invalid credentials")
        })
    }

    register = () => {
        if(document.getElementById('register_username').value==""){
            alert("All fields are mandatory")
            console.log("3")
            return
        }
        if(document.getElementById('register_email').value==""){
            alert("All fields are mandatory")
            console.log("4")
            return
        }
        if(document.getElementById('register_name').value==""){
            alert("All fields are mandatory")
            console.log("5")
            return
        }
        if(document.getElementById('register_type').value==""){
            alert("All fields are mandatory")
            console.log("6")
            return
        }
        if(document.getElementById('register_password').value==""){
            alert("All fields are mandatory")
            console.log("7")
            return
        }
        
        
        let user = {
            username: document.getElementById('register_username').value,
            email: document.getElementById('register_email').value,
            name: document.getElementById('register_name').value,
            type: document.getElementById('register_type').value,
            password: document.getElementById('register_password').value
        }
        axios.post('https://proiect-web-sanduac.c9users.io/register', user).then((res) => {
            if(res.status === 200){
                console.log(res.data)
                alert("successfully registered")
                this.showLogionLayout()
            }
        }).catch((err) =>{
            console.log(err)
            alert("invalid credentials")
        })
    }
        

    render() {
        return (
            <React.Fragment>
            <div id="login">
                <div id="options">
                    <button onClick={this.showLogionLayout}>LOGIN Option</button>
                    <button onClick={this.showRegisterLayout}>REGISTER Option</button>
                </div>
                <div id="login_layout">
                    <input id="login_username" type="text" placeholder="username"/>
                    <input id="login_password" type="text" placeholder="password"/>
                    <button onClick={this.login}>LOGIN</button>
                </div>
                <div id="register_layout">
                    <input id="register_name" type="text" placeholder="name"/>
                    <input id="register_email" type="text" placeholder="email"/>
                    <input id="register_username" type="text" placeholder="username"/>
                    <input id="register_password" type="text" placeholder="password"/>
                    <input id="register_type" type="text" placeholder="type"/>
                    <button onClick={this.register}>REGISTER</button>
                </div>
            </div>
            </React.Fragment>
        )
    }

}
