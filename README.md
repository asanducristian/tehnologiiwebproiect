**About us**

1. Monica Buruiana
2. Saftii Bogdan
3. Sandu Alexandru

Stundeti ai grupei 1074 in cadrul Academiei de Studii Economice Bucuresti, facultatea de Cibernetica si Informatica Economica sectia Informatica Economica, promotia 2019.

---

**Descrierea proiectului**

Tema proiectului nostru este cea cu numarul 14 si anume "Organizare turnee de jocuri integrat cu IGBD". Platforma pe care dorim sa o dezvoltam o sa combine atat usurinta folosirii de catre utilizator cat si complexitatea algoritmica pentru a oferi jucatorilor o experienta unica. Platforma noastra va gazdui turnee pentru unul din jocurile de pe platforma IGBD (https://www.igdb.com/discover). Juctorii se vor inscrie pentru participarea la un turneu iar in momentul in care sunt destui jucatori de rank asemanator se va crea turneul. Jucatorii vor trebui sa respecte si sa dispute toate meciurile pentru castgarea turneului. Acesta se va termina cand toate meciurile au fost jucate. In functie de punctajul obtinut aceastia vor fi clasati pe platforma cu un anumit rank. 

---

**Specificatii detaliate**

Platforma va folosi un serviciu rest pentru partea de comunicare client server. 

---

ROOT USER: saftii
DATABASE NAME: c9

---

Guidelines:

1. Am facut o colectie de postman cu toate APIurile care sunt de forma baseURL/{ruta}. BaseURL este o variabila din enviroment. Fiecare api are descriere in care se specifica si fieldurile mandatorii.
Colectia: https://www.getpostman.com/collections/93dd204608e546c96970
2. Pentru rulare trebuie schimbat rootuser la initializarea bazei de date
3. Toate APIurile de acolo sunt functionale iar raspunsurile lor sunt JSON de tip: {"success": true/false, "message":{STRING}, "data":{JSON OBJECT/ARRAY}}

---

Mistakes were made, turnament is actually Tournament, semantics...


